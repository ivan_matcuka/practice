from documenter import Documenter
from wikipedist import Wikipedist

documenter = Documenter('document.docx')
wikipedist = Wikipedist(documenter.opened_document.tables[0])
documenter.check_fields(wikipedist.pages)
documenter.opened_document.save('result.docx')
