from docx import Document


class Documenter:
    sections = ['География', 'История', 'Население', 'Достопримечательности']

    def __init__(self, name):
        self.opened_document = Document(name)
        self.fix_district()

    def fix_district(self):
        for i, row in enumerate(self.opened_document.tables[0].rows):
            if i < 2:
                continue
            district = row.cells[3].text
            row.cells[3].text = district.replace(' ', '')
            row.cells[3].text = district.replace('район', '')

    def check_fields(self, pages):
        for i, page in enumerate(pages):
            if page:
                for j, section in enumerate(self.sections):
                    result = self.check_section(page, section)
                    if section == 'География' and result == '-':
                        result = self.check_section(page, 'Географическое положение')
                    self.opened_document.tables[0].rows[i + 2].cells[6 + j].text = result

    @staticmethod
    def check_section(page, section):
        if page.section(section):
            return 'v'
        return '-'
