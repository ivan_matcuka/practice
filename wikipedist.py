import wikipedia


class Wikipedist:
    pages = []
    towns = []

    def __init__(self, table):
        wikipedia.set_lang('ru')
        self.table = table
        self.get_towns()
        self.get_pages()

    def get_towns(self):
        for i, row in enumerate(self.table.rows):
            if i < 2:
                continue
            town = row.cells[4].text
            self.towns.append(town)

    def get_pages(self):
        for i, town in enumerate(self.towns):
            try:
                part = self.table.rows[i + 2].cells[2].text
                title = town + ' (' + part + ')'
                page = wikipedia.page(title=title, pageid=None, auto_suggest=False, redirect=True)
            except Exception:
                try:
                    part = self.table.rows[i + 2].cells[3].text
                    title = town + ' (' + part + ' район)'
                    page = wikipedia.page(title=title, pageid=None, auto_suggest=False, redirect=True)
                except Exception:
                    try:
                        page = wikipedia.page(title=town, pageid=None, auto_suggest=False, redirect=True)
                    except Exception:
                        page = None
            self.pages.append(page)
